<?php

include_once "Interface/Calc.php";
abstract class Operation implements Calc
{
    protected $answer;

    public function round( $getValue): Calc
    {
        $this->answer = round($this->answer,$getValue,1);
        return $this;
    }

    public function answer() :float
    {
        return $this->answer;
    }
}
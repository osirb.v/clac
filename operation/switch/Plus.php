<?php

class Plus extends operation
{
public function calc(float $firstNumber, float $secondNumber)
{
    $this->answer = $firstNumber+$secondNumber;
    return $this;
}
}
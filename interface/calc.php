<?php

interface Calc
{
    public function calc(float $firstNumber,float $secondNumber);

    public function round( $getValue):Calc;

    public function answer():float;
}
